"""
Examples of how to retrieve data stored in time-series, or metadata about them.
"""
from archipelagos.common.data import get_yyyy_mm_dd, get_list, get_yyyy_mm_dd_hh_mm_ss_n, get_yyyy_mm_dd_hh_mm_ss
from archipelagos.common.data.timeseries import TimeSeriesRequest, TimeSeriesMetadataRequest
from archipelagos import Archipelagos

from pandas import Timestamp
import pandas as pd
import json
import math


def time_series_metadata_api_example():
    """
    Example of using the API to retrieve time-series metadata.
    """
    # Build the request to send to the platform

    request = TimeSeriesMetadataRequest("ELEXON", "FUEL", "HH")

    # Send the request to the platform

    response = Archipelagos.send(request)

    # Inspect the response received from the platform

    if response.error_occurred:
        print("\nThe platform returned an error code ({}) and message: \"{}\"".format(response.error_code, response.error_message))
    else:
        print("\nThe platform returned the following metadata for the time-series {}/{}/{} via the API:\n".format(response.metadata.source, response.metadata.category, response.metadata.label))
        print("Source: {}".format(response.metadata.source))
        print("Category: {}".format(response.metadata.category))
        print("Label: {}".format(response.metadata.label))
        print("URL: {}".format(response.metadata.url))
        print("Summary: \"{}\"".format(response.metadata.summary))
        print("Description: \"{}\"".format(response.metadata.description))
        print("Frequency: {}".format(response.metadata.frequency.name))
        print("Features: {}".format(get_list(list(response.metadata.features.keys()))))
        print("Created: {}".format(get_yyyy_mm_dd_hh_mm_ss_n(response.metadata.created)))
        print("Edited: {}".format(get_yyyy_mm_dd_hh_mm_ss_n(response.metadata.edited)))
        print("Refreshed: {}".format(get_yyyy_mm_dd_hh_mm_ss_n(response.metadata.refreshed)))
        print("Oldest Date/Time: {}".format(get_yyyy_mm_dd_hh_mm_ss_n(response.metadata.oldest_date_time)))
        print("Newest Date/Time: {}".format(get_yyyy_mm_dd_hh_mm_ss_n(response.metadata.newest_date_time)))


def time_series_api_example():
    """
    Example of using the API to retrieve time-series data.
    """
    # Build the request to send to the platform

    request = TimeSeriesRequest("ELEXON", "FUEL", "HH", Timestamp(2022, 6, 14), Timestamp(2022, 6, 15), ["CCGT", "OIL", "COAL", "NUCLEAR", "WIND"], ascending=True)

    # Send the request to the platform

    response = Archipelagos.send(request)

    # Inspect the response received from the platform

    if response.error_occurred:
        print("\nThe platform returned an error code ({}) and message: \"{}\"".format(response.error_code, response.error_message))
    else:
        print("\nThe platform returned the following data for the time-series {}/{}/{} via the API:\n".format(response.data.metadata.source,
                                                                                                              response.data.metadata.category,
                                                                                                              response.data.metadata.label))
        for date_time, prices in response.data.data.flattened_data:
            if prices:
                ccgt = prices["CCGT"]
                oil = prices["OIL"]
                coal = prices["COAL"]
                nuclear = prices["NUCLEAR"]
                wind = prices["WIND"]
                print("Date: {}\t\tCCGT: {}\t\tOil: {}\t\tNuclear: {}\t\tWind: {}".format(date_time, ccgt, oil, coal, nuclear, wind))
            else:
                print("Date: {}".format(date_time.split("T")[0]))


def time_series_metadata_http_example():
    """
    Example of using HTTP to retrieve time-series metadata.
    """
    # Build the request to send to the platform

    params = {"type": "time-series-metadata",
              "source": "ELEXON",
              "category": "FUEL",
              "label": "HH",
              "format": "json"}
    request = Archipelagos.build_url(params)

    # Send the request to the platform

    response = Archipelagos.send(request)
    json_response = json.loads(response)

    # Inspect the response received from the platform

    if json_response["error_occurred"]:
        print("\nThe platform returned an error code ({}) and message: \"{}\"".format(json_response["error_code"], json_response["error_message"]))
    else:
        metadata = json_response["metadata"]
        source = metadata["source"]
        category = metadata["category"]
        label = metadata["label"]

        print("\nThe platform returned the following metadata for the time-series {}/{}/{} via HTTP:\n".format(source, category, label))
        print("Source: {}".format(source))
        print("Category: {}".format(category))
        print("Label: {}".format(label))
        print("URL: {}".format(metadata["url"]))
        print("Summary: \"{}\"".format(metadata["summary"]))
        print("Description: \"{}\"".format(metadata["description"]))
        print("Frequency: {}".format(metadata["frequency"]))
        print("Features: {}".format(get_list(list(metadata["features"].keys()))))
        print("Created: {}".format(metadata["created"]))
        print("Edited: {}".format(metadata["edited"]))
        print("Refreshed: {}".format(metadata["refreshed"]))
        print("Oldest Date/Time: {}".format(metadata["oldest_date_time"]))
        print("Newest Date/Time: {}".format(metadata["newest_date_time"]))


def time_series_http_example():
    """
    Example of using HTTP to retrieve time-series data.
    """
    # Build the request to send to the platform

    params = {"type": "time-series",
              "source": "ELEXON",
              "category": "FUEL",
              "label": "HH",
              "start": "2020-1-1",
              "end": "2020-1-2",
              "features": "CCGT,OIL,COAL,NUCLEAR,WIND",
              "format": "json",
              "flatten": "True",
              "order": "asc",
              "api_key": Archipelagos.api_key}

    request = Archipelagos.build_url(params)

    # Send the request to the platform

    response = Archipelagos.send(request)
    json_response = json.loads(response)

    # Inspect the response received from the platform

    if json_response["error_occurred"]:
        print("\nThe platform returned an error code ({}) and message: \"{}\"".format(json_response["error_code"], json_response["error_message"]))
    else:
        print("\nThe platform returned the following data for the time-series {}/{}/{} via HTTP:\n".format(
            json_response["metadata"]["source"], json_response["metadata"]["category"], json_response["metadata"]["label"]))

        for date_time, prices in json_response["data"]:
            if prices:
                ccgt = prices["CCGT"]
                oil = prices["OIL"]
                coal = prices["COAL"]
                nuclear = prices["NUCLEAR"]
                wind = prices["WIND"]
                print("Date: {}\t\tCCGT: {}\t\tOil: {}\t\tCoal: {}\t\tNuclear: {}\t\tWind: {}".format(date_time, ccgt, oil, coal, nuclear, wind))
            else:
                print("Date: {}".format(date_time.split("T")[0]))


def time_series_get_pandas():
    """
    Example of using get to retrieve time-series data into a Pandas Data Frame.
    """
    # Specify the time-series to return

    source = "ELEXON"
    category = "FUEL"
    label = "HH"

    start = pd.Timestamp(2020, 1, 1)
    end = pd.Timestamp(2020, 1, 2)

    features = ["CCGT", "OIL", "COAL", "NUCLEAR", "WIND"]

    # Send the request to the platform

    df = Archipelagos.get_time_series(source, category, label, start, end, features=features)

    # Inspect the response received from the platform

    print("\nThe platform returned a Pandas Data Frame with the following data for the time-series {}/{}/{}:\n".format(source, category, label))

    for date_time, row in df.iterrows():
        ccgt = row["CCGT"]
        oil = row["OIL"]
        coal = row["COAL"]
        nuclear = row["NUCLEAR"]
        wind = row["WIND"]
        print("Date: {}\t\tCCGT: {}\t\tOil: {}\t\tCoal: {}\t\tNuclear: {}\t\tWind: {}".format(get_yyyy_mm_dd_hh_mm_ss(date_time), ccgt, oil, coal, nuclear, wind))


def time_series_get_numpy():
    """
    Example of using get to retrieve time-series data into a Numpy array.
    """
    # Specify the time-series to return

    source = "ELEXON"
    category = "FUEL"
    label = "HH"

    start = pd.Timestamp(2020, 1, 1)
    end = pd.Timestamp(2020, 1, 2)

    features = ["CCGT", "OIL", "COAL", "NUCLEAR", "WIND"]

    # Send the request to the platform

    array = Archipelagos.get_time_series(source, category, label, start, end, features=features, returns="numpy")

    # Inspect the response received from the platform

    print("\nThe platform returned a Numpy array with the following data for the time-series {}/{}/{}:\n".format(source, category, label))

    for date_time, ccgt, oil, coal, nuclear, wind in array:
        print("Date: {}\t\tCCGT: {}\t\tOil: {}\t\tCoal: {}\t\tNuclear: {}\t\tWind: {}".format(get_yyyy_mm_dd_hh_mm_ss(date_time), ccgt, oil, coal, nuclear, wind))


def time_series_get_weather_data():
    """
    Example of retrieving weather data from the U.K. Met Office.
    """
    # Specify the time-series to return

    source = "MET_OFFICE"
    category = "WEATHER_STATION"
    label = "CARDIFF"

    # Send the request to the platform

    df = Archipelagos.get_time_series(source, category, label)

    # Inspect the response received from the platform

    print("\nThe platform returned a Pandas Data Frame with the following data for the time-series {}/{}/{}:\n".format(source, category, label))

    for date_time, row in df.iterrows():
        mean_max_temp = row.get("MeanMaxTemp", None)
        mean_min_temp = row.get("MeanMinTemp", None)
        air_frost_days = row.get("AirFrostDays", None)
        total_rainfall = row.get("TotalRainfall", None)
        sunshine_duration = row.get("SunshineDuration", None)

        print("Date: {}\t\tMax Temp: {: <6}\t\tMin Temp: {: <6}\t\tAir Frost Days: {: <6}\t\t"
              "Total Rainfall: {: <6}\t\tSunshine Duration: {: <6}".format(get_yyyy_mm_dd(date_time), mean_max_temp, mean_min_temp, air_frost_days, total_rainfall, sunshine_duration))


def time_series_get_commodity_prices():
    """
    Example of retrieving commodity price data from the World Bank.
    """
    # Specify the time-series to return

    source = "WORLD_BANK"
    category = "COMMODITY"
    label = "PRICES"

    # Send the request to the platform

    df = Archipelagos.get_time_series(source, category, label)

    # Inspect the response received from the platform

    print("\nThe platform returned a Pandas Data Frame with the following data for the time-series {}/{}/{}:\n".format(source, category, label))

    for date_time, row in df.iterrows():
        price_str = ""

        for commodity, price in row.items():
            if math.isnan(price):
                price = "N/A"
            price_str += "{}: {:<8}\t\t".format(commodity, price)

        print("Date: {}\t\t{}".format(get_yyyy_mm_dd(date_time), price_str))


if __name__ == "__main__":
    # An API Key can be provided for all requests as below, or it can be provided (overridden) in individual requests

    Archipelagos.api_key = "YOUR-API-KEY"

    # Send a time-series request,and metadata request, via the Client API

    time_series_metadata_api_example()
    time_series_api_example()

    # Send a time-series request,and metadata request, via HTTP

    time_series_metadata_http_example()
    time_series_http_example()

    # An example of using get to return time-series data as a Pandas Data Frame or Numpy array

    time_series_get_pandas()
    time_series_get_numpy()

    time_series_get_weather_data()
    time_series_get_commodity_prices()
