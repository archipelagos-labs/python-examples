"""
Examples of how to retrieve data stored in file stores, or metadata about them.
"""
from archipelagos.common.data.filestore import FileStoreRequest, FileStoreMetadataRequest, FileMetadataRequest
from archipelagos.common.data import get_yyyy_mm_dd_hh_mm_ss_n
from archipelagos import Archipelagos

import json


def file_store_metadata_api_example():
    """
    Example of using the API to retrieve file store metadata.
    """
    # Build the request to send to the platform

    request = FileStoreMetadataRequest("ELEXON", "FUEL", "HH")

    # Send the request to the platform

    response = Archipelagos.send(request)

    # Inspect the response received from the platform

    if response.error_occurred:
        print("\nThe platform returned an error code ({}) and message: \"{}\"".format(response.error_code, response.error_message))
    else:
        print("\nThe platform returned the following metadata for the file store {}/{}/{} via the API:\n".format(response.metadata.source, response.metadata.category, response.metadata.label))
        print("Source: {}".format(response.metadata.source))
        print("Category: {}".format(response.metadata.category))
        print("Label: {}".format(response.metadata.label))
        print("URL: {}".format(response.metadata.url))
        print("Summary: \"{}\"".format(response.metadata.summary))
        print("Description: \"{}\"".format(response.metadata.description))
        print("Premium: \"{}\"".format(response.metadata.premium))
        print("Created: {}".format(get_yyyy_mm_dd_hh_mm_ss_n(response.metadata.created)))
        print("Edited: {}".format(get_yyyy_mm_dd_hh_mm_ss_n(response.metadata.edited)))


def file_store_api_example():
    """
    Example of using the API to retrieve a file from a file store.
    """
    # Build the request to send to the platform

    request = FileStoreRequest("ELEXON", "FUEL", "HH", "fuelhh_2021.csv")

    # Send the request to the platform

    response = Archipelagos.send(request)

    # Inspect the response received from the platform

    if response.error_occurred:
        print("\n\n\nThe platform returned an error code ({}) and message: \"{}\"".format(response.error_code, response.error_message))
    else:
        print("\n\n\nThe platform returned the following data for the file '{}' from the file store {}/{}/{} via the API:\n\n{}\n".format(
            response.metadata.name, response.metadata.source, response.metadata.category, response.metadata.label, response.file.decode('utf-8')))


def file_metadata_api_example():
    """
    Example of using the API to retrieve file metadata.
    """
    # Build the request to send to the platform

    request = FileMetadataRequest("ELEXON", "FUEL", "HH", ".")

    # Send the request to the platform

    response = Archipelagos.send(request)

    # Inspect the response received from the platform

    if response.error_occurred:
        print("\n\nThe platform returned an error code ({}) and message: \"{}\"".format(response.error_code, response.error_message))
    else:
        print("\nThe platform returned the following metadata for files in the file store {}/{}/{} via the API:\n".format(request.source, request.category, request.label))

        for file_metadata in response.metadata:
            print("Source: {}\t\tCode: {}\t\tID: {}\t\tName: {: <18}\t\tSize: {: <10}\t\tPremium: {}\t\tCreated: {}".format(
                file_metadata.source, file_metadata.category, file_metadata.label, file_metadata.name, file_metadata.size, file_metadata.premium, file_metadata.created))


def file_store_metadata_http_example():
    """
    Example of using HTTP to retrieve file store metadata.
    """
    # Build the request to send to the platform

    params = {"type": "file-store-metadata",
              "source": "ELEXON",
              "category": "FUEL",
              "label": "HH",
              "format": "json"}
    request = Archipelagos.build_url(params)

    # Send the request to the platform

    response = Archipelagos.send(request)
    json_response = json.loads(response)

    # Inspect the response received from the platform

    if json_response["error_occurred"]:
        print("\nThe platform returned an error code ({}) and message: \"{}\"".format(json_response["error_code"], json_response["error_message"]))
    else:
        metadata = json_response["metadata"]
        source = metadata["source"]
        category = metadata["category"]
        label = metadata["label"]

        print("\nThe platform returned the following metadata for the file store {}/{}/{} via HTTP:\n".format(source, category, label))
        print("Source: {}".format(source))
        print("Category: {}".format(category))
        print("Label: {}".format(label))
        print("URL: {}".format(metadata["url"]))
        print("Summary: \"{}\"".format(metadata["summary"]))
        print("Description: \"{}\"".format(metadata["description"]))
        print("Premium: {}".format(metadata["premium"]))
        print("Created: {}".format(metadata["created"]))
        print("Edited: {}\n".format(metadata["edited"]))


def file_store_http_example():
    """
    Example of using HTTP to retrieve a file from a file store.
    """
    # Build the request to send to the platform

    params = {"type": "file-store",
              "source": "ELEXON",
              "category": "FUEL",
              "label": "HH",
              "filename": "fuelhh_2021.csv",
              "format": "json"}
    request = Archipelagos.build_url(params)

    # Send the request to the platform

    response = Archipelagos.send(request)
    json_response = json.loads(response)

    # Inspect the response received from the platform

    if json_response["error_occurred"]:
        print("\n\nThe platform returned an error code ({}) and message: \"{}\"".format(json_response["error_code"], json_response["error_message"]))
    else:
        print("\n\nThe platform returned the following data for the file '{}' from the file store {}/{}/{} via HTTP:\n\n{}\n".format(
            json_response["metadata"]["name"], json_response["metadata"]["source"], json_response["metadata"]["category"], json_response["metadata"]["label"], json_response["file"]))


def file_metadata_http_example():
    """
    Example of using HTTP to retrieve file metadata.
    """
    # Build the request to send to the platform

    params = {"type": "file-metadata",
              "source": "ELEXON",
              "category": "FUEL",
              "label": "HH",
              "pattern": "",
              "format": "json"}
    request = Archipelagos.build_url(params)

    # Send the request to the platform

    response = Archipelagos.send(request)
    json_response = json.loads(response)

    # Inspect the response received from the platform

    if json_response["error_occurred"]:
        print("\nThe platform returned an error code ({}) and message: \"{}\"".format(json_response["error_code"], json_response["error_message"]))
    else:
        metadata = json_response["metadata"]
        print("\nThe platform returned the following metadata for files in the file store {}/{}/{} via HTTP:\n".format(params["source"], params["category"], params["label"]))

        for file_metadata in metadata:
            print("Source: {}\t\tCategory: {}\t\tLabel: {}\t\tName: {: <18}\t\tSize: {: <10}\t\tPremium: {}\t\tCreated: {}".format(
                file_metadata["source"], file_metadata["category"], file_metadata["label"], file_metadata["name"],
                file_metadata["size"], file_metadata["premium"], file_metadata["created"]))


def file_get_pandas():
    """
    Example of using get to retrieve a .csv file into a Pandas DataFrame.
    """
    # Specify the file to return

    source = "ELEXON"
    category = "FUEL"
    label = "HH"
    name = "fuelhh_2021.csv"

    # Send the request to the platform

    df = Archipelagos.get_file(source, category, label, name)

    # Inspect the response received from the platform

    print("\n\n\nThe platform returned a Pandas DataFrame with the following data for the file '{}' from the file store {}/{}/{}:\n".format(name, source, category, label))
    print(df.head())


def file_get_geopandas():
    """
    Example of using get to retrieve a Shapefile (saved as a compressed directory) into a GeoPandas DataFrame.
    """
    # Specify the file to return

    source = "OS"
    category = "OPEN_DATA"
    label = "BOUNDARY_LINE"
    name = "GreaterLondonConstRegion.zip"

    # Send the request to the platform

    df = Archipelagos.get_file(source, category, label, name, returns='geopandas')

    # Inspect the response received from the platform

    message = "\n\n\nThe platform returned a GeoPandas GeoDataFrame with the following data for the file '{}' from the file store {}/{}/{}:\n"
    print(message.format(name, source, category, label))
    print(df.head())


if __name__ == "__main__":
    # An API Key can be provided for all requests as below, or it can be provided (overridden) in individual requests

    Archipelagos.api_key = "YOUR-API-KEY"

    # Send a file store request,and metadata requests, via the Client API

    file_store_metadata_api_example()
    file_store_api_example()
    file_metadata_api_example()

    # Send a file store request,and metadata requests, via HTTP

    file_store_metadata_http_example()
    file_store_http_example()
    file_metadata_http_example()

    # Examples of using get to return collection data as a Pandas Data Frame, GeoPandas Data Frame or Numpy array

    file_get_pandas()
    file_get_geopandas()
