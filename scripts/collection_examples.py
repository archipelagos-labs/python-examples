"""
Examples of how to retrieve data stored in collections, or metadata about them.
"""
from archipelagos.common.data.collection import CollectionRequest, CollectionMetadataRequest
from archipelagos.common.data import get_list, get_yyyy_mm_dd_hh_mm_ss_n
from archipelagos import Archipelagos

import json


def collection_metadata_api_example():
    """
    Example of using the API to retrieve collection metadata.
    """
    # Build the object representing the request (an API Key can be specified, but not in this example)

    request = CollectionMetadataRequest("WRI", "GPD", "1-3-0")

    # Send the request to the platform and inspect the response

    response = Archipelagos.send(request)

    if response.error_occurred:
        message = "\nThe platform returned an error code ({}) and message: \"{}\""
        print(message.format(response.error_code, response.error_message))
    else:
        print("\nThe platform returned the following metadata for the collection {}/{}/{} via the API:\n".format(response.metadata.source, response.metadata.category, response.metadata.label))
        print("Source: {}".format(response.metadata.source))
        print("Category: {}".format(response.metadata.category))
        print("Label: {}".format(response.metadata.label))
        print("URL: {}".format(response.metadata.url))
        print("Summary: \"{}\"".format(response.metadata.summary))
        print("Description: \"{}\"".format(response.metadata.description))
        print("Features: {}".format(get_list(list(response.metadata.features.keys()))))
        print("Premium: \"{}\"".format(response.metadata.premium))
        print("Created: {}".format(get_yyyy_mm_dd_hh_mm_ss_n(response.metadata.created)))
        print("Edited: {}".format(get_yyyy_mm_dd_hh_mm_ss_n(response.metadata.edited)))
        print("Refreshed: {}".format(get_yyyy_mm_dd_hh_mm_ss_n(response.metadata.refreshed)))


def collection_api_example():
    """
    Example of using the API to retrieve collection data.
    """
    # Build the request to send to the platform

    features = ["country_long", "primary_fuel", "capacity_mw", "longitude", "latitude", "name"]
    filters = {"country": "= 'GBR'", "capacity_mw": "> 5.0"}
    request = CollectionRequest("WRI", "GPD", "1-3-0", features=features, filters=filters)

    # Send the request to the platform

    response = Archipelagos.send(request)

    # Inspect the response received from the platform

    if response.error_occurred:
        print("\nThe platform returned an error code ({}) and message: \"{}\"".format(response.error_code, response.error_message))
    else:
        print("\nThe platform returned the following data for the collection {}/{}/{} via the API:\n".format(response.data.metadata.source, response.data.metadata.category, response.data.metadata.label))

        for features in response.data.data.flattened_data:
            if len(features):
                country = features["country_long"]
                fuel = features["primary_fuel"]
                capacity = features["capacity_mw"]
                longitude = features["longitude"]
                latitude = features["latitude"]
                name = features["name"]
                print("Country: {:<15}\t\tFuel: {:<10}\t\tCapacity: {:<7}\t\tLongitude: {:<7}\t\tLatitude: {:<7}\t\tName: {}".format(country, fuel, capacity, longitude, latitude, name))


def collection_metadata_http_example():
    """
    Example of using HTTP to retrieve collection metadata.
    """
    # Build the request to send to the platform

    params = {"type": "collection-metadata",
              "source": "WRI",
              "category": "GPD",
              "label": "1-3-0",
              "format": "json"}
    request = Archipelagos.build_url(params)

    # Send the request to the platform

    response = Archipelagos.send(request)
    json_response = json.loads(response)

    # Inspect the response received from the platform

    if json_response["error_occurred"]:
        print("\nThe platform returned an error code ({}) and message: \"{}\"".format(json_response["error_code"], json_response["error_message"]))
    else:
        metadata = json_response["metadata"]
        source = metadata["source"]
        category = metadata["category"]
        label = metadata["label"]

        print("\nThe platform returned the following metadata for the collection {}/{}/{} via HTTP:\n".format(source, category, label))
        print("Source: {}".format(source))
        print("Category: {}".format(category))
        print("Label: {}".format(label))
        print("URL: {}".format(metadata["url"]))
        print("Summary: \"{}\"".format(metadata["summary"]))
        print("Description: \"{}\"".format(metadata["description"]))
        print("Features: {}".format(get_list(list(metadata["features"].keys()))))
        print("Created: {}".format(metadata["created"]))
        print("Edited: {}".format(metadata["edited"]))
        print("Refreshed: {}".format(metadata["refreshed"]))


def collection_http_example():
    """
    Example of using HTTP to retrieve collection data.
    """
    # Build the request to send to the platform

    params = {"type": "collection",
              "source": "WRI",
              "category": "GPD",
              "label": "1-3-0",
              "features": "country_long,primary_fuel,capacity_mw,longitude,latitude,name",
              "format": "json",
              "flatten": "True",
              "country": "= 'GBR'",
              "capacity_mw": "> 5.0",
              "api_key": Archipelagos.api_key}

    request = Archipelagos.build_url(params)

    # Send the request to the platform

    response = Archipelagos.send(request)
    json_response = json.loads(response)

    # Inspect the response received from the platform

    if json_response["error_occurred"]:
        print("\nThe platform returned an error code ({}) and message: \"{}\"".format(json_response["error_code"], json_response["error_message"]))
    else:
        print("\nThe platform returned the following data for the collection {}/{}/{} via HTTP:\n".format(
            json_response["metadata"]["source"], json_response["metadata"]["category"], json_response["metadata"]["label"]))

        for features in json_response["data"]:
            if len(features):
                country = features["country_long"]
                fuel = features["primary_fuel"]
                capacity = features["capacity_mw"]
                longitude = features["longitude"]
                latitude = features["latitude"]
                name = features["name"]
                print("Country: {:<15}\t\tFuel: {:<10}\t\tCapacity: {:<7}\t\tLongitude: {:<7}\t\tLatitude: {:<7}\t\tName: {}".format(country, fuel, capacity, longitude, latitude, name))


def collection_get_pandas():
    """
    Example of using get to retrieve collection data into a Pandas Data Frame.
    """
    # Specify the collection to return

    source = "WRI"
    category = "GPD"
    label = "1-3-0"

    # Send the request to the platform

    features = ["country_long", "primary_fuel", "capacity_mw", "longitude", "latitude", "name"]
    filters = {"country": "= 'GBR'", "capacity_mw": "> 5.0"}
    df = Archipelagos.get_collection(source, category, label, features=features, filters=filters)

    # Inspect the response received from the platform

    print("\nThe platform returned a Pandas Data Frame with the following data for the collection {}/{}/{}:\n".format(source, category, label))

    for object_id, row in df.iterrows():
        country = row["country_long"]
        fuel = row["primary_fuel"]
        capacity = row["capacity_mw"]
        longitude = row["longitude"]
        latitude = row["latitude"]
        name = row["name"]
        print("Country: {:<15}\t\tFuel: {:<10}\t\tCapacity: {:<7}\t\tLongitude: {:<7}\t\tLatitude: {:<7}\t\tName: {}".format(country, fuel, capacity, longitude, latitude, name))


def collection_get_numpy():
    """
    Example of using get to retrieve collection data into a Numpy array.
    """
    # Specify the collection to return

    source = "WRI"
    category = "GPD"
    label = "1-3-0"

    # Send the request to the platform

    filters = {"country": "= 'GBR'", "capacity_mw": "> 5.0"}
    features = ["country_long", "primary_fuel", "capacity_mw", "longitude", "latitude", "name"]
    array = Archipelagos.get_collection(source, category, label, features=features, filters=filters, returns="numpy")

    # Inspect the response received from the platform

    print("\nThe platform returned a Numpy array with the following data for the collection {}/{}/{}:\n".format(source, category, label))

    for object_id, country, fuel, capacity, longitude, latitude, name in array:
        print("Country: {:<15}\t\tFuel: {:<10}\t\tCapacity: {:<7}\t\tLongitude: {:<7}\t\tLatitude: {:<7}\t\tName: {}".format(country, fuel, capacity, longitude, latitude, name))


if __name__ == "__main__":
    # An API Key can be provided for all requests as below, or it can be provided (overridden) in individual requests

    Archipelagos.api_key = "YOUR-API-KEY"

    # Send a collection request,and metadata request, via the Client API

    collection_metadata_api_example()
    collection_api_example()

    # Send a collection request,and metadata request, via HTTP

    collection_metadata_http_example()
    collection_http_example()

    # Examples of using get to return collection data as a Pandas Data Frame or Numpy array

    collection_get_pandas()
    collection_get_numpy()
