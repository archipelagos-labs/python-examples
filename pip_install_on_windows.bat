
curl --silent -o Fiona-1.8.20-cp39-cp39-win_amd64.whl https://gitlab.com/api/v4/projects/34586148/repository/files/resources%%2FFiona-1.8.20-cp39-cp39-win_amd64%%2Ewhl/raw
curl --silent -o GDAL-3.4.1-cp39-cp39-win_amd64.whl https://gitlab.com/api/v4/projects/34586148/repository/files/resources%%2FGDAL-3.4.1-cp39-cp39-win_amd64%%2Ewhl/raw
curl --silent -o geopandas-0.10.2-py2.py3-none-any.whl https://gitlab.com/api/v4/projects/34586148/repository/files/resources%%2Fgeopandas-0.10.2-py2.py3-none-any%%2Ewhl/raw
curl --silent -o pyproj-3.3.0-cp39-cp39-win_amd64.whl https://gitlab.com/api/v4/projects/34586148/repository/files/resources%%2Fpyproj-3.3.0-cp39-cp39-win_amd64%%2Ewhl/raw
curl --silent -o Shapely-1.8.0-cp39-cp39-win_amd64.whl https://gitlab.com/api/v4/projects/34586148/repository/files/resources%%2FShapely-1.8.0-cp39-cp39-win_amd64%%2Ewhl/raw

call pip install GDAL-3.4.1-cp39-cp39-win_amd64.whl
call pip install pyproj-3.3.0-cp39-cp39-win_amd64.whl
call pip install Fiona-1.8.20-cp39-cp39-win_amd64.whl
call pip install Shapely-1.8.0-cp39-cp39-win_amd64.whl
call pip install geopandas-0.10.2-py2.py3-none-any.whl

call pip install archipelagos==0.0.15
call pip install geopandas==0.10.2
call pip install jupyterlab==3.3.2
call pip install matplotlib==3.5.1
call pip install numpy==1.21.2
call pip install pandas==1.3.5
call pip install protobuf==3.19.1
call pip install requests==2.27.1
call pip install statsmodels==0.13.2

del Fiona-1.8.20-cp39-cp39-win_amd64.whl
del GDAL-3.4.1-cp39-cp39-win_amd64.whl
del geopandas-0.10.2-py2.py3-none-any.whl
del pyproj-3.3.0-cp39-cp39-win_amd64.whl
del Shapely-1.8.0-cp39-cp39-win_amd64.whl