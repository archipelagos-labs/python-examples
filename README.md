
## 1. Introduction

This project provides examples of using the Archipelagos Python Client, and instructions/scripts that may be used to install the Python packages that the examples depend upon.
The below sections discuss how to configure a suitable virtual environment to run the examples, and also provide an overview of the different examples available.<br>


## 2. Build Environment

The project home directory contains an [Anaconda](https://www.anaconda.com) environment file *archipelagos-examples.yml* that will install all the Python packages required to run the examples.
Inside the same directory there is also a *requirements.txt* file equivalent to *archipelagos-examples.yml* that may be used to import packages using [pip](https://pypi.org/project/pip).
<br><br>
The *archipelagos-examples.yml* file can be utilised by running the command ```conda env create -f archipelagos-examples.yml``` in an Anaconda Prompt. Alternatively,
the *requirements.txt* file can be used by running ```pip install -r requirements.txt``` inside a suitable Python virtual ('virtualenv') environment.<br>
<br>
[GeoPandas](https://geopandas.org), required to run some examples, cannot be installed directly on Windows platforms using pip. If you wish to run the examples on 
Windows while using [pip](https://pypi.org/project/pip), the script *pip_install_on_windows.bat* can be run inside a Python virtual ('virtualenv') environment to install all required dependencies.<br>


## 3. Running Examples

Examples of using the Python Client within a Python script are located in the *scripts* subdirectory. Each script is self-contained and does not depend on another file. 
To run a script type ```python script_name.py``` in a virtual environment built as described in Section 2 above. 
<br><br>
Examples of using the Python Client and visualising the data retrieved within a [Jupyter Notebook](https://jupyter.org) can be found within the *notebooks* subdirectory. 
The notebooks should also be run in a virtual environment built as described in Section 2 above.<br>
<br>